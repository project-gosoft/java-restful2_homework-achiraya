package com.test.demo;

import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller // This means that this class is a Controller
//@RequestMapping(path = "/demo")
public class MainController {
	@RequestMapping("/")
	@ResponseBody
	String home() {
		return "Hello World!";
	}

	@Autowired
	private ShopRepository shopRepository;
	@Autowired
	private ManagerRepository managerRepository;

	@GetMapping(path = "/shop")
	public @ResponseBody Iterable<Shop> getAllUsers1() {
		// This returns a JSON or XML with the users
		return shopRepository.findAll();
	}

	@PostMapping(path = "/shop/add") // Map ONLY POST Requests
	public @ResponseBody String addNewUser(@RequestParam String shop) {
		Shop n = new Shop();
		n.setShop(shop);
		shopRepository.save(n);
		return "Saved";
	}

	@PutMapping(path = "shop/edit", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String editShop(@RequestBody Shop shop) {
		shop.setShop(shop.getShop());
		shopRepository.save(shop);
		return "Saved";
	}

	@DeleteMapping("/shop/delete/{shop_id}")
	@ResponseBody
	String delete(@PathVariable("shop_id") int shopId) {
		if (!shopRepository.findById(shopId).isEmpty()) {
			shopRepository.deleteById(shopId);
		}
		return "Delete shop :" + shopId;
	}

	// Computer//

	@GetMapping(path = "/computer")
	public @ResponseBody Iterable<Shop> getAllUsers() {
		return shopRepository.findAll();
	}

	@PostMapping(path = "/computer/add") // Map ONLY POST Requests
	public @ResponseBody String addNewUsers(@RequestParam String shop, @RequestParam String computer,
			@RequestParam String staff, @RequestParam String manager) {
		Shop n = new Shop();
		Manager m = new Manager();
		n.setShop(shop);
		n.setComputer(computer);
		n.setStaff(staff);
		m.setName(manager);
		n.setManager(m);
		shopRepository.save(n);
		managerRepository.save(m);
		return "Saved";
	}

	@PutMapping(path = "computer/edit", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String editComputer(@RequestBody Shop computer) {
		computer.setShop(computer.getShop());
		shopRepository.save(computer);
		return "Saved";
	}

	@DeleteMapping(path = "/delete/{id}")
	@ResponseBody
	public String delete1(@PathVariable("id") int shopId) {
		if (!shopRepository.findById(shopId).isEmpty()) {
			shopRepository.deleteById(shopId);
		}
		return "Delete shop :" + shopId;
	}

	// Manager//

	@GetMapping(path = "/manager")
	public @ResponseBody Iterable<Manager> getAllUsers2() {
		return managerRepository.findAll();
	}

	@PostMapping(path = "/manager/add") // Map ONLY POST Requests
	public @ResponseBody String addManager(@RequestParam String manager) {
		Manager n = new Manager();
		n.setName(manager);
		managerRepository.save(n);
		return "Saved";
	}

	@PutMapping(path = "manager/edit", consumes = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody String editManager(@RequestBody Manager manager) {
		manager.setName(manager.getName());
		managerRepository.save(manager);
		return "Saved";
	}

	@DeleteMapping(path = "manager/delete/{managerId}")
	@ResponseBody
	public String deleteManager(@PathVariable("managerId") int id) {
		if (!managerRepository.findById(id).isEmpty()) {
			managerRepository.deleteById(id);
		}
		return "Delete manager :" + id;
	}
}
